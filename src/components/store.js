import React from "react";

const StoreItem = props => (
  <li>
    <b>£{props.item.price}</b> {props.item.name}
    <button onClick={(e) => props.newBasketItemHandler(props.item, e)}>+</button>
  </li>
);

const StoreItems = props => (
  <ul>
    {props.storeItems.map(item => (
      <StoreItem
        item={item}
        newBasketItemHandler={props.newBasketItemHandler}
        key={item.name} />
    ))}
  </ul>
);

const StoreContainer = props => {
  return (
    <div>
      <h1>{props.header}</h1>
      <StoreItems storeItems={props.storeItems}
      newBasketItemHandler={props.newBasketItemHandler} />
    </div>
  );
};

export function Store(props) {
  return (
    <div>
      <StoreContainer 
      header="Shopping Items" 
      storeItems={props.storeItems}
      newBasketItemHandler={props.newBasketItemHandler}
       />
    </div>
  );
}
