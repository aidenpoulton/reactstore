import React from "react";
import { Store } from "./store";
import { Basket } from "./Basket/basket";

export class ShopController extends React.Component {
    constructor() {
        super()
        this.state = { basketItems: null };
        this.newItemHandler = this.newItemHandler.bind(this);
    }

    newItemHandler(param, e) {
        param.quantity = 1;
        if (this.state.basketItems) {
            const matchedItem = this.state.basketItems.filter(item => item.name === param.name)[0];
            var newItems = this.state.basketItems;
            if (matchedItem) {
                newItems[newItems.indexOf(matchedItem)].quantity++;
            } else {
                newItems = newItems.concat(param)
            }
            this.setState({ basketItems: newItems });
        } else {
            this.setState({ basketItems: [param] })
        }
    }

    render() {
        return (
            <div>
                <Store
                    storeItems={[
                        { name: "Apples", price: 0.22 },
                        { name: "Lemons", price: 0.33 },
                        { name: "Bananas", price: 0.44 }
                    ]}
                    newBasketItemHandler={this.newItemHandler} />
                <Basket
                    basketItems={this.state.basketItems}
                />
            </div>
        );
    }
}
