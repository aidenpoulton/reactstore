import React from "react";

const BasketItem = props => {
  return (<li>
    {props.item.quantity} x <b>{props.item.name}</b> at £{props.item.price}
  </li>)
}

const BasketItems = props => {
  if (props.basketItems) {
    return (
      <ul>
        {props.basketItems.map(item => (
          <BasketItem 
          item={item}
          key={item.name} />
        ))}
      </ul>
    )
  } else {
    return (<div>No items in the basket yet!</div>)
  }
}

export class Basket extends React.Component {

  totalBasketItems() {
    var total = 0;
    if (this.props.basketItems) {
      this.props.basketItems.map(item =>
        (
          total = total + item.price
        )
      )
    }
    return (total.toFixed(2));
  }

  render() {
    return (<div><h1>Basket Total : £{this.totalBasketItems()}</h1>
      <BasketItems basketItems={this.props.basketItems} />
    </div>)
  }
}
