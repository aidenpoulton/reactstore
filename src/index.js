import React from "react";
import ReactDOM from "react-dom";
import { ShopController } from "./components/shopController"

function App() {
  return (
    <div className="App">
      <ShopController />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
